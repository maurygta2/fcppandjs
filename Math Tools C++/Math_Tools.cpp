#include <iostream>
#include <ctime>
#include <array>
#include <math.h>
#include <string>
#define point float
using namespace std;

string foo [2] = {"3","2"};
array<string,3> myarray {"2","4","7"};

// Matemática funções
class Math {
    public:
        double mul(double param,double param2) {
            return param*param2;
        }
        double divi(double param,double param2) {
            return param/param2;
        }
        double add(double param,double param2) {
            return param+param2;
        }
        double sub(double param,double param2) {
            return param-param2;
        }
        double pon(double param,double param2) {
            return pow(param,param2);
        }
        double raizq(double param) {
            return sqrt(param);
        }
        double raizc(double param) {
            return cbrt(param);
        }
        double pri() {
            return PI;
        }
        Math() {
            PI = 3.1415926535;
        }
    private:
        double PI;
};

int main() {
    Math Calc;
	cout << "Value: "<< Calc.pri() << endl;
	return 0;
}