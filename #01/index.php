<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <meta charset="utf-8">
  </head>
  <body>
	<section>
      <h3>Escolha o pacote desejado</h3><hr>
        Iniciante<input type="radio" name="radio" id="iniciante" checked /><br>
        Avançado<input type="radio" name="radio" id="avancado" />
         <h4>Quantidade de Aulas:</h4>
         <input id="price" class="slider" type="range" min="1" max="30" value="1"/>
          <span id="resultado1" style="font-size: 25px;"></span>

      <h4>Total:</h4>
        <p id="resultado2">Preço: </p>
</section>
<script>
var p = document.getElementById("price"),
    res1 = document.getElementById("resultado1"),
    res2 = document.getElementById("resultado2"),
    curso_iniciante = document.getElementById("iniciante"),
    curso_avancado = document.getElementById("avancado");
function update_price() {
    var curso = (curso_iniciante.checked) ? 20 
    : (curso_avancado.checked) ? 40 : null;
    res2.innerHTML = "Preço: "+(p.value * curso) + ",00R$";
}
p.addEventListener("input", function () {
    res1.innerHTML = p.value;
}, false);
p.addEventListener("change", update_price, true);
curso_iniciante.addEventListener("click",update_price,false)
curso_avancado.addEventListener("click",update_price,false);
</script>
  </body>
</html>