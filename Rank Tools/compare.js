function rank(value) {
    // verificar se é objeto
    if (typeof value == 'object') {
        // ex.: pos = length(2)+1 => pos é 3;
        let pos = value.length+1;
        // checar cada propriedade
        for (let f in value) {
            // verifica se é numero
            if (isNaN(value[0]-value[f])) {
                return "Erro";
            }
            // ex: compare = value[0](2)-value[f](3) => compare é -1
            let compare = value[0]-value[f];
            // se for 0 dimunuir
            if (compare === 0) {
                pos -= 1;
            }
            // se o numero for negativo dimunuir
            else if (compare < 0) {
                pos -= 1;
            }
        }
        // retorna a posiçao do index 0
        return pos;
    }
    return "Erro";
}
console.log(rank([7,3,6]))