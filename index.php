<!DOCTYPE html>
<html>
    <body>
        <input type="time" id="data">
        <button onclick="minutes()">Result</button>
        <p id="value"></p>
        <script>
            var data = document.getElementById("data");
            var value = document.getElementById("value");
            function minutes() {
                let f0 = data.value.split(":");
                value.innerHTML = ((Number(f0[0])*60)+Number(f0[1])) + " Minutes";
                return 0;
            }
        </script>
    </body>
</html>