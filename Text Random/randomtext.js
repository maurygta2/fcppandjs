var Random_Text = (param) => {
    this.f1 = [];
    this.f3 = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","r","s","t","u","v","w","x","y","z"];
    for (this.f0=0;this.f0<param;this.f0++) {
        this.f1.push(Math.floor(Math.random()*(this.f3.length)));
    }
    this.f2 = "";
    for (let f0 in this.f1) {
        this.f4 = (Math.floor(Math.random()*2) == 1) ? "up" : "down";
        if (this.f4 == "down") {
            this.f2 += this.f3[this.f1[f0]];
        } else {
            this.f2 += String(this.f3[this.f1[f0]]).toUpperCase();
        }
    }
    return this.f2;
};