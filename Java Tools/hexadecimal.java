/*
Criada por Maury e Daniel
*/
import java.io.*;

class base16 {
    public String toHexa(int num) {
        String _value = "";
        String[] base = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
        int _00 = base.length;
        int _01 = 1;
        while (num >= _00) {
            _00 *= base.length;
            _01 += 1;
        }
        for (int i = 0;i < _01;i++) {
            _00 = _00 / base.length;
            int _02 = (int) Math.floor(num/_00)%base.length;
            _value += base[_02];
        }
        return _value;
    }
}

public class Main {
	public static void main(String[] args) {
	    base16 test = new base16();
		System.out.println(test.toHexa(3));
	}
}