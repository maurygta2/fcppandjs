class Primo 
{
    boolean isPrimo;
    public Primo(int value) 
    {
        if (value < 2)
        {
            isPrimo = false;
            return;
        }
        if (value == 2)
        {
            isPrimo = true;
            return;
        }
        if (value%2 == 0) {
            isPrimo = false;
            return;
        }
        int limit = (int)Math.ceil(value/2);
        for (int i = 3;i<=limit;i+=2)
        {
            if (value%i == 0)
            {
                isPrimo = false;
                return;
            }
        }
        isPrimo = true;
    }
}
public class MyClass
{
    public static void main(String args[])
    {
        Primo num = new Primo(8);
        System.out.println(num.isPrimo);
    }
}