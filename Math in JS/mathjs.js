function _Math(val) {
    let _val = (typeof val == "number") ? val : 0;
    this.val = () => {return _val;};
    let methods = {
        add: function(val_) {_val += val_; return methods;},
        sub: function(val_) {_val -= val_; return methods;},
        mul: function(val_) {_val *= val_; return methods;},
        div: function(val_) {_val /= val_; return methods;},
        mod: function(val_) {_val %= val_; return methods;},
        cbrt: function() {_val = Math.cbrt(_val); return methods;},
        sqrt: function() {_val = Math.sqrt(_val); return methods;}
    };
    this.add = methods.add;
    this.sub = methods.sub;
    this.mul = methods.mul;
    this.div = methods.div;
    this.mod = methods.mod;
    this.cbrt = methods.cbrt;
    this.sqrt  = methods.sqrt;
}
var math = new _Math(3);
console.log(math.add(2).add(1).sub(2).mul(4).sqrt());
console.log(math.a());
