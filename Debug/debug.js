﻿const debug = {
    obj: document.getElementById("debug"),
    log: function(param) {
        this.obj.innerHTML += "class.js <a style='white'>log</a>: "+param+"<br>";
    },
    error: function(param) {
        this.obj.innerHTML += "class.js <a style='color: red;'>error</a>: "+param+"<br>";
    },
    info: function(param) {
        this.obj.innerHTML += "class.js <a style='color : blue;'>info</a>: "+param+"<br>";
    },
    start : function() {
        document.body.style.backgroundColor = "black";
        this.obj.style.color = 'white';
        this.obj.style.border = "5px solid #FFFFFF";
    }
};
debug.start();
